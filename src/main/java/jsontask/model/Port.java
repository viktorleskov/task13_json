package jsontask.model;

public class Port {
    private String portName;

    public Port() {
    }

    public Port(String name) {
        this.portName = name;
    }

    public String getPortName() {
        return portName;
    }

    public void setPortName(String portName) {
        this.portName = portName;
    }

    @Override
    public String toString() {
        return "Port{" +
                "name='" + portName + '\'' +
                '}';
    }
}
