package jsontask.model.device;


import jsontask.model.Type;

public class Device {
    private int price;
    private boolean critical;
    private String name;
    private String origin;
    private Type types;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Type getType() {
        return types;
    }

    public void setType(Type type) {
        this.types = type;
    }

    public boolean isCritical() {
        return critical;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    @Override
    public String toString() {
        return "Device{" +
                "name='" + name + '\'' +
                ", origin='" + origin + '\'' +
                ", price=" + price +
                ", type=" +  types.toString()+
                ", critical=" + critical +
                "}\n";
    }
}
