package jsontask.parser;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import jsontask.model.device.Device;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class GsonParseTest {
    public static void main(String[] args) {
        Gson gson = new Gson();
        JsonReader jsonReader;
        JsonParser jsonParser = new JsonParser();
        String json = null;
        try {
            json = new String(Files.readAllBytes(Paths.get("src/main/resources/json/devices1.json")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert json != null;
        JsonElement jsonTree = jsonParser.parse(json);
        try {
            jsonReader = new JsonReader(new FileReader("src/main/resources/json/devices1.json"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Device[] devices = gson.fromJson(json, Device[].class);

//        JsonArray jsonArray = jsonParser.parse(json).getAsJsonObject().getAsJsonArray();
//        jsonArray.forEach(System.out::println);

    }
}
