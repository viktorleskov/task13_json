import com.google.gson.Gson;
import jsontask.model.device.Device;
import jsontask.model.device.Devices;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TestApp {
    @Test
    public void givenInvalidInput__whenValidating__thenInvalid() throws ValidationException, NullPointerException {
        JSONObject jsonSchema = new JSONObject(
                new JSONTokener(TestApp.class.getResourceAsStream("scheme.json")));
        JSONObject jsonSubject = new JSONObject(
                new JSONTokener(TestApp.class.getResourceAsStream("devices.json")));

        Schema schema = SchemaLoader.load(jsonSchema);
        schema.validate(jsonSubject);
    }

    @Test
    public void parseJsonTest2() throws ValidationException {
        String json = null;
        try {
            json = json = new String(Files.readAllBytes(Paths.get("src/main/resources/json/devicesArray.json")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        System.out.println(json);
        Device[] devices = gson.fromJson(json, Device[].class);
        System.out.println(devices.length);
    }
}
